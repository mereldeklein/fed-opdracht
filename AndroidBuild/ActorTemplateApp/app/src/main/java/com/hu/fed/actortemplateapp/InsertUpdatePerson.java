package com.hu.fed.actortemplateapp;

import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.Manifest;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * Created by Merel on 17-3-2017.
 */

public class InsertUpdatePerson extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {
    String key;
    DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
    private Person person;
    private Boolean updating;
    private String templateKey;
    private String templateName;
    private String personKey;
    private String TAG = "InsertUpdateTemplate";
    private static final int REQUEST_CAMERA = 0;
    private static final int SELECT_FILE = 1;
    private static final int READ_PERMISSION = 2;
    private static final int WRITE_PERMISSION = 3;
    private ImageButton imageButton;
    private Uri selectedImage;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private String mUsername;
    private String mPhotoUrl;
    public static final String ANONYMOUS = "anonymous";
    private GoogleApiClient mGoogleApiClient;

    private Context writePermissionContext;
    private Bitmap writePermissionBitmap;

    private TextView naam, functie, email, telefoon, aantekingen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_update_person);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mUsername = ANONYMOUS;

        // Initialize Firebase Auth
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        if (mFirebaseUser == null) {
            // Not signed in, launch the Sign In activity
            startActivity(new Intent(this, SignInActivity.class));
            finish();
            return;
        } else {
            mUsername = mFirebaseUser.getDisplayName();
            if (mFirebaseUser.getPhotoUrl() != null) {
                mPhotoUrl = mFirebaseUser.getPhotoUrl().toString();
            }
        }

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API)
                .build();

        Intent intent = getIntent();
        updating = intent.getBooleanExtra("updatingPerson", false);
        templateKey = intent.getStringExtra("templateKey");
        templateName = intent.getStringExtra("templateName");

        TextView templateNameField = (TextView) findViewById(R.id.templateName);
        templateNameField.setText(templateName);

        imageButton = (ImageButton) findViewById(R.id.personImage);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showImageDialog();
            }
        });

        FloatingActionButton finishButton = (FloatingActionButton) findViewById(R.id.finishButton);
        finishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!updating){
                    addPerson();
                }
                else if(updating){
                    updatePerson();
                }
            }
        });

        naam = (TextView) findViewById(R.id.editNaam);
        functie = (TextView) findViewById(R.id.editFunctie);
        email = (TextView) findViewById(R.id.editEmail);
        telefoon = (TextView) findViewById(R.id.editTel);
        aantekingen = (TextView) findViewById(R.id.editNotes);

        if(updating){
            personKey = intent.getStringExtra("personKey");

            DatabaseReference mDatabasePerson = FirebaseDatabase.getInstance().getReference();
            mDatabasePerson.child("actoren").child(personKey).addListenerForSingleValueEvent(
                    new ValueEventListener() {

                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            Person p = dataSnapshot.getValue(Person.class);
                            p.key = dataSnapshot.getKey();
                            person = p;

                            naam.setText(person.getName());
                            functie.setText(person.getFunctie());
                            email.setText(person.getEmail());
                            telefoon.setText(person.getTelefoon());
                            aantekingen.setText(person.getAantekeningen());

                            String fileURL = "userImages/"+person.key+".jpg";

                            FirebaseStorage storage = FirebaseStorage.getInstance();
                            StorageReference storageRef = storage.getReferenceFromUrl("gs://actortemplateapp-10e30.appspot.com/").child(fileURL);

                            try {
                                final File localFile = File.createTempFile("images", "jpg");
                                storageRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                                    @Override
                                    public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                                        Bitmap bitmap = BitmapFactory.decodeFile(localFile.getAbsolutePath());
                                        imageButton.setImageBitmap(bitmap);

                                            //HIER
                                        checkWritePermission(findViewById(R.id.personImage).getContext(), bitmap);
//                                        selectedImage = getImageUri(findViewById(R.id.personImage).getContext(), bitmap);

                                        Log.e(TAG, ""+selectedImage);
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception exception) {
                                    }
                                });
                            } catch (IOException e ) {}
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    }
            );
        }

        Log.e(TAG, "updating person: "+updating+", template key: "+templateKey);
    }

    private void showImageDialog(){
        final CharSequence[] items = { "Camera", "Bestaande foto",
                "Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(InsertUpdatePerson.this);
        builder.setTitle("Kies een afbeelding");

        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Camera")) {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        startActivityForResult(takePictureIntent, REQUEST_CAMERA);
                    }
                }
                else if (items[item].equals("Bestaande foto")) {
                    checkReadPermission();
                }
                else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch(requestCode) {
            case REQUEST_CAMERA:
                if(resultCode == RESULT_OK){
                    Bundle extras = imageReturnedIntent.getExtras();
                    Bitmap imageBitmap = (Bitmap) extras.get("data");
                    imageButton.setImageBitmap(imageBitmap);

                    //HIER
                    checkWritePermission(this, imageBitmap);
//                    selectedImage = getImageUri(this, imageBitmap);

                    Log.e(TAG, ""+selectedImage);
                }

                break;
            case SELECT_FILE:
                if(resultCode == RESULT_OK){
                    selectedImage = imageReturnedIntent.getData();
                    imageButton.setImageURI(selectedImage);

                    Log.e(TAG, ""+selectedImage);
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case READ_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto , SELECT_FILE);

                }
                else {
                    // permission denied, boo!
                }
                return;
            }
            case WRITE_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    selectedImage = getImageUri(writePermissionContext, writePermissionBitmap);

                }
                else {
                    // permission denied, boo!
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void checkReadPermission(){
        // Als READ_EXTERNAL_STORAGE geen permission heeft
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {

            }
            else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, READ_PERMISSION);
            }
        }
        //Als READ_EXTERNAL_STORAGE al wel permission heeft
        else if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(pickPhoto , SELECT_FILE);
        }
    }

    private void checkWritePermission(Context context, Bitmap bitmap){
        // Als WRITE_EXTERNAL_STORAGE geen permission heeft
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

            }
            else {
                // No explanation needed, we can request the permission.
                writePermissionContext = context;
                writePermissionBitmap = bitmap;
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, WRITE_PERMISSION);
            }
        }
        //Als READ_EXTERNAL_STORAGE al wel permission heeft
        else if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            selectedImage = getImageUri(context, bitmap);
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    private void addPerson(){
        if(naam.getText().toString().trim().isEmpty() && email.getText().toString().trim().isEmpty()){
            Snackbar snackbar = Snackbar.make(findViewById(R.id.editNaam), "Voer een naam of e-mail adress in", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
        else{
            person = new Person();
            person.setName(naam.getText().toString());
            person.setFunctie(functie.getText().toString());
            person.setEmail(email.getText().toString());
            person.setTelefoon(telefoon.getText().toString());
            person.setAantekeningen(aantekingen.getText().toString());
            person.setTemplatekey(templateKey);

            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child("actoren").push();
            mDatabase.setValue(person);

            String newKey = mDatabase.getKey();
            String fileURL = "userImages/"+newKey+".jpg";

            if(selectedImage != null){
                StorageReference storageRef = FirebaseStorage.getInstance().getReference().child(fileURL);
                Uri file = selectedImage;
                UploadTask uploadTask = storageRef.putFile(file);
            }
            else{
                Resources resources = getResources();
                StorageReference storageRef = FirebaseStorage.getInstance().getReference().child(fileURL);
                Uri file = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE+"://"+resources.getResourcePackageName(R.drawable.default_user)+ '/' + getResources().getResourceTypeName(R.drawable.default_user) + '/' + getResources().getResourceEntryName(R.drawable.default_user));
                UploadTask uploadTask = storageRef.putFile(file);
            }

            finish();
        }
    }

    private void updatePerson(){
        if(naam.getText().toString().trim().isEmpty() && email.getText().toString().trim().isEmpty()){
            Snackbar snackbar = Snackbar.make(findViewById(R.id.editNaam), "Voer een naam of e-mail adress in", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
        else {
            person.setName(naam.getText().toString());
            person.setFunctie(functie.getText().toString());
            person.setEmail(email.getText().toString());
            person.setTelefoon(telefoon.getText().toString());
            person.setAantekeningen(aantekingen.getText().toString());
            person.setTemplatekey(templateKey);

            FirebaseDatabase.getInstance().getReference("actoren/"+person.key).setValue(person);

            String fileURL = "userImages/"+person.key+".jpg";

            if(selectedImage != null){
                StorageReference storageRef = FirebaseStorage.getInstance().getReference().child(fileURL);
                Uri file = selectedImage;
                UploadTask uploadTask = storageRef.putFile(file);
            }
            else{
                Resources resources = getResources();
                StorageReference storageRef = FirebaseStorage.getInstance().getReference().child(fileURL);
                Uri file = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE+"://"+resources.getResourcePackageName(R.drawable.default_user)+ '/' + getResources().getResourceTypeName(R.drawable.default_user) + '/' + getResources().getResourceEntryName(R.drawable.default_user));
                UploadTask uploadTask = storageRef.putFile(file);
            }

            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_show_projects, menu);

        if (mFirebaseUser != null) {
            mUsername = mFirebaseUser.getDisplayName();
            if (mFirebaseUser.getPhotoUrl() != null) {
                mPhotoUrl = mFirebaseUser.getPhotoUrl().toString();
                try {
                    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                    StrictMode.setThreadPolicy(policy);
                    URL url = new URL(mPhotoUrl);
                    Bitmap image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                    menu.getItem(0).setIcon(new BitmapDrawable(getResources(), image));
                } catch (IOException e) {
                    System.out.println(e);
                }

//                menu.getItem(0).setIcon(new BitmapDrawable(getResources(), map));
//                Log.e(TAG, "3");
//                Log.e(TAG, mPhotoUrl);
            }
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sign_out_menu:
                mFirebaseAuth.signOut();
                Auth.GoogleSignInApi.signOut(mGoogleApiClient);
                mUsername = ANONYMOUS;
                startActivity(new Intent(this, SignInActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
        Toast.makeText(this, "Google Play Services error.", Toast.LENGTH_SHORT).show();
    }
}
