package com.hu.fed.actortemplateapp;

/**
 * Created by Merel on 13-3-2017.
 */

public class ActorTemplate {
    private String description;
    private String role;
    private String projectkey;
    private String status;
    String key;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getProjectkey() {
        return projectkey;
    }

    public void setProjectkey(String projectkey) {
        this.projectkey = projectkey;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
