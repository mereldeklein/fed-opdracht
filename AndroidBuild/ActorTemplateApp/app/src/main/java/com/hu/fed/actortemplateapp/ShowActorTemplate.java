package com.hu.fed.actortemplateapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.net.URL;

/**
 * Created by Merel on 16-3-2017.
 */

public class ShowActorTemplate extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {
    String key;
    DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
    private ActorTemplate template;
    private Boolean isAnalyst;
    private Project project;
    private RecyclerView recyclerView;
    private DetailPersonAdapter mAdapter;
    private String TAG = "ShowActorTemplate";
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private String mUsername;
    private String mPhotoUrl;
    public static final String ANONYMOUS = "anonymous";
    private GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_actor_template);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mUsername = ANONYMOUS;

        // Initialize Firebase Auth
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        if (mFirebaseUser == null) {
            // Not signed in, launch the Sign In activity
            startActivity(new Intent(this, SignInActivity.class));
            finish();
            return;
        } else {
            mUsername = mFirebaseUser.getDisplayName();
            if (mFirebaseUser.getPhotoUrl() != null) {
                mPhotoUrl = mFirebaseUser.getPhotoUrl().toString();
            }
        }

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API)
                .build();

        Intent intent = getIntent();
        isAnalyst = intent.getBooleanExtra("isAnalyst", false);
        key = intent.getStringExtra("templateKey");
        if(key != null && isAnalyst != null){
            Log.e(TAG, "Loaded with key from intent");
            setUp(key);
        }
        else if(SharedValues.getInstance().getTemplate().key != null){
            Log.e(TAG, "Loaded with key from SharedValues");
            key = SharedValues.getInstance().getTemplate().key;
            isAnalyst = SharedValues.getInstance().getProject().isAnalyst();
            setUp(key);
        }
    }

    private void setUp(String key){
//        mDatabase.child("templates").child(key).addListenerForSingleValueEvent(
        mDatabase.child("templates").child(key).addValueEventListener(
                new ValueEventListener() {

                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        TextView tv1 = (TextView) findViewById(R.id.actorTitle);
                        TextView tv2 = (TextView) findViewById(R.id.actorDescription);

                        ActorTemplate t = dataSnapshot.getValue(ActorTemplate.class);
                        t.key = dataSnapshot.getKey();
                        template = t;

                        //Tempate setten in sharedvalues
                        SharedValues.getInstance().setTemplate(template);

                        tv1.setText(template.getRole());
                        tv2.setText(template.getDescription());

                        //Persons list
                        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

                        mAdapter = new DetailPersonAdapter(template, isAnalyst);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                        recyclerView.setLayoutManager(mLayoutManager);
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        recyclerView.setAdapter(mAdapter);

                        //Analyst button visibility
                        if(isAnalyst){
                            //Add person button
                            FloatingActionButton add = (FloatingActionButton) findViewById(R.id.addPersonButton);
                            add.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    addPerson();
                                }
                            });
                            add.setVisibility(View.VISIBLE);

                            //Edit tempate button
                            FloatingActionButton edit = (FloatingActionButton) findViewById(R.id.editTemplateButton);
                            edit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    editTemplate();
                                }
                            });
                            edit.setVisibility(View.VISIBLE);

                            //Archive template button
                            FloatingActionButton archive = (FloatingActionButton) findViewById(R.id.archiveTemplateButton);
                            archive.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    archiveTemplate();
                                }
                            });
                            archive.setVisibility(View.VISIBLE);
                        }

                        mDatabase.child("projects").child(template.getProjectkey()).addListenerForSingleValueEvent(
                                new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        TextView tv3 = (TextView) findViewById(R.id.projectTitle);
                                        TextView tv4 = (TextView) findViewById(R.id.projectDescription);

                                        Project p = dataSnapshot.getValue(Project.class);
                                        p.key = dataSnapshot.getKey();
                                        project = p;

                                        tv3.setText(project.getName());
                                        tv4.setText(project.getDescription());

                                        //Kijkt of de ingelogde gebruiker analyst of teamlid van het project is,
                                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                                        if (user != null) {
                                            String email = user.getEmail();
                                            if(p.getAnalysts().containsValue(email)){
                                                p.setAnalyst(true);
                                                project.setAnalyst(true);
                                            }
                                            else if(p.getTeam().containsValue(email)){
                                                p.setAnalyst(false);
                                                project.setAnalyst(false);
                                            }
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                }
                        );
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                }
        );
    }

    private void addPerson(){
        Intent intent = new Intent(this, InsertUpdatePerson.class);
        intent.putExtra("updatingPerson", false);
        intent.putExtra("templateKey", template.key);
        intent.putExtra("templateName", template.getRole());
        startActivity(intent);
    }

    private void editTemplate(){
        Intent intent = new Intent(this, InsertUpdateTemplate.class);

        intent.putExtra("key", template.getProjectkey());
        intent.putExtra("updating", true);
        intent.putExtra("templateKey", template.key);

        startActivity(intent);
    }

    private void archiveTemplate(){
        FirebaseDatabase.getInstance().getReference("templates/"+template.key+"/status").setValue("ARCHIVED");
        onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_show_projects, menu);

        if (mFirebaseUser != null) {
            mUsername = mFirebaseUser.getDisplayName();
            if (mFirebaseUser.getPhotoUrl() != null) {
                mPhotoUrl = mFirebaseUser.getPhotoUrl().toString();
                try {
                    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                    StrictMode.setThreadPolicy(policy);
                    URL url = new URL(mPhotoUrl);
                    Bitmap image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                    menu.getItem(0).setIcon(new BitmapDrawable(getResources(), image));
                } catch (IOException e) {
                    System.out.println(e);
                }
            }
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sign_out_menu:
                mFirebaseAuth.signOut();
                Auth.GoogleSignInApi.signOut(mGoogleApiClient);
                mUsername = ANONYMOUS;
                startActivity(new Intent(this, SignInActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
        Toast.makeText(this, "Google Play Services error.", Toast.LENGTH_SHORT).show();
    }
}
