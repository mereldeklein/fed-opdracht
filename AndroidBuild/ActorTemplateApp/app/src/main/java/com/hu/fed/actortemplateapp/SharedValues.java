package com.hu.fed.actortemplateapp;

/**
 * Created by Merel on 16-3-2017.
 */

public class SharedValues {
    private static SharedValues sharedValues;
    private Project project;
    private ActorTemplate template;

    private SharedValues(){

    }

    public static SharedValues getInstance(){
        if(sharedValues == null){
            sharedValues = new SharedValues();
        }
        return sharedValues;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public ActorTemplate getTemplate() {
        return template;
    }

    public void setTemplate(ActorTemplate template) {
        this.template = template;
    }
}
