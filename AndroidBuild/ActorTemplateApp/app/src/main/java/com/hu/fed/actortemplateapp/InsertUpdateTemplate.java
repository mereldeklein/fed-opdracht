package com.hu.fed.actortemplateapp;

import android.content.ContentResolver;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.w3c.dom.Text;

import java.io.IOException;
import java.net.URL;


/**
 * Created by Tessa den Oudsten on 20-3-2017.
 */

public class InsertUpdateTemplate extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener{
    private String TAG = "InsertUpdateTemplate";
    private String mUsername;
    public static final String ANONYMOUS = "anonymous";
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private GoogleApiClient mGoogleApiClient;
    private String mPhotoUrl;
    private ActorTemplate template;
    private boolean updating = false;
    String key;
    private String templateKey;
    private ActorTemplate existingTemplate;

    private TextView actor, beschrijving;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_update_template);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mUsername = ANONYMOUS;

        // Initialize Firebase Auth
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        if (mFirebaseUser == null) {
            // Not signed in, launch the Sign In activity
            startActivity(new Intent(this, SignInActivity.class));
            finish();
            return;
        } else {
            mUsername = mFirebaseUser.getDisplayName();
            if (mFirebaseUser.getPhotoUrl() != null) {
                mPhotoUrl = mFirebaseUser.getPhotoUrl().toString();
            }
        }

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API)
                .build();

        Intent intent = getIntent();
        key = intent.getStringExtra("key");
        updating = intent.getBooleanExtra("updating", false);

        loadProject(key);

        actor = (TextView) findViewById(R.id.actorName);
        beschrijving = (TextView) findViewById(R.id.editBeschrijving);

        FloatingActionButton finishButton = (FloatingActionButton) findViewById(R.id.finishButton);
        finishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!updating){
                    addTemplate();
                }
                else if(updating){
                    updateTemplate();
                }
            }
        });

        if(updating) {
            templateKey = intent.getStringExtra("templateKey");
            DatabaseReference templateRef = FirebaseDatabase.getInstance().getReference();
            templateRef.child("templates").child(templateKey).addListenerForSingleValueEvent(
                    new ValueEventListener() {

                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            ActorTemplate t = dataSnapshot.getValue(ActorTemplate.class);
                            t.key = dataSnapshot.getKey();
                            existingTemplate = t;

                            actor.setText(existingTemplate.getRole());
                            beschrijving.setText(existingTemplate.getDescription());
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    }
            );
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_show_projects, menu);

        if (mFirebaseUser != null) {
            mUsername = mFirebaseUser.getDisplayName();
            if (mFirebaseUser.getPhotoUrl() != null) {
                mPhotoUrl = mFirebaseUser.getPhotoUrl().toString();
                try {
                    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                    StrictMode.setThreadPolicy(policy);
                    URL url = new URL(mPhotoUrl);
                    Bitmap image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                    menu.getItem(0).setIcon(new BitmapDrawable(getResources(), image));
                } catch (IOException e) {
                    System.out.println(e);
                }
            }
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sign_out_menu:
                mFirebaseAuth.signOut();
                Auth.GoogleSignInApi.signOut(mGoogleApiClient);
                mUsername = ANONYMOUS;
                startActivity(new Intent(this, SignInActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
        Toast.makeText(this, "Google Play Services error.", Toast.LENGTH_SHORT).show();
    }

    private void addTemplate(){
        Log.e(TAG, "ADD THE TEMPLATE");
        if(beschrijving.getText().toString().trim().isEmpty() || actor.getText().toString().trim().isEmpty()){
            Log.e(TAG, "Het is leeg!!");

            Snackbar snackbar = Snackbar.make(findViewById(R.id.editBeschrijving), "Voer een Actor en/of beschrijving in", Snackbar.LENGTH_LONG);
            snackbar.show();

        }
        //Velden ingevuld
        else{
            ActorTemplate template1 = new ActorTemplate();
            template1.setDescription(beschrijving.getText().toString());
            template1.setRole(actor.getText().toString());
            template1.setStatus("ACTIVE");
            //projectkey is de key van het project waar je in zit
            template1.setProjectkey(key);
            Log.d(TAG, template1.toString());

            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child("templates").push();
            mDatabase.setValue(template1);

            finish();
        }
    }

    private void updateTemplate(){
        if(beschrijving.getText().toString().trim().isEmpty() || actor.getText().toString().trim().isEmpty()){
            Snackbar snackbar = Snackbar.make(findViewById(R.id.editBeschrijving), "Voer een Actor en/of beschrijving in", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
        else {
            existingTemplate.setRole(actor.getText().toString());
            existingTemplate.setDescription(beschrijving.getText().toString());

            FirebaseDatabase.getInstance().getReference("templates/"+existingTemplate.key).setValue(existingTemplate);

            finish();
        }
    }

    public void loadProject(String key){
        DatabaseReference projectRef = FirebaseDatabase.getInstance().getReference();
        projectRef.child("projects").child(key).addListenerForSingleValueEvent(
                new ValueEventListener() {

                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        TextView tv1 = (TextView)findViewById(R.id.projectName);
                        TextView tv2 = (TextView) findViewById(R.id.projectDescription);

                        Project p = dataSnapshot.getValue(Project.class);
                        p.key = dataSnapshot.getKey();

                        tv1.setText(p.getName());
                        tv2.setText(p.getDescription());
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                }
        );
    }
}
