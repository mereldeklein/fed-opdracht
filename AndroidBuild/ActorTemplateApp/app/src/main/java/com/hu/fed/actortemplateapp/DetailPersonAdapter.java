package com.hu.fed.actortemplateapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.icu.text.DateFormat;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Merel on 17-3-2017.
 */

public class DetailPersonAdapter extends RecyclerView.Adapter<DetailPersonAdapter.MyViewHolder> {

    private List<Person> personsList = new ArrayList<>();
    private DatabaseReference mFirebaseDatabaseReference;
    private String TAG = "DetailPersonAdapter";
    private ActorTemplate template;
    private Boolean isAnalyst;
    private HashMap<String, ImageView> imageViews = new HashMap<String, ImageView>();

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView name, function, email, tel, description;
        public Button edit;
        public ImageView image;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.personName);
            function = (TextView) view.findViewById(R.id.personFunction);
            email = (TextView) view.findViewById(R.id.personEmail);
            tel = (TextView) view.findViewById(R.id.personTel);
            description = (TextView) view.findViewById(R.id.personDescription);
            edit = (Button) view.findViewById(R.id.editPersonButton);
            image = (ImageView) view.findViewById(R.id.imageView2);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int pos = getAdapterPosition();
            Person person = personsList.get(pos);

//            Intent intent = new Intent(v.getContext(), InsertUpdatePerson.class);
//            intent.putExtra("updatingPerson", true);
//            intent.putExtra("templateKey", template.key);
//            intent.putExtra("templateName", template.getRole());
//            intent.putExtra("personKey", person.key);
//            Log.e(TAG, person.key);
//            v.getContext().startActivity(intent);
        }
    }


    public DetailPersonAdapter(ActorTemplate t, Boolean analyst) {
        template = t;
        isAnalyst = analyst;

        final DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();

        mDatabase.child("actoren").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Person person = dataSnapshot.getValue(Person.class);
                person.key = dataSnapshot.getKey();
                //Alleen tonen bij template hoort
                if(person.getTemplatekey().equals(template.key)){
                    personsList.add(person);
                    notifyDataSetChanged();
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Person person = dataSnapshot.getValue(Person.class);
                person.key = dataSnapshot.getKey();

                updatePerson(person);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                personsList.remove( dataSnapshot.getValue(ActorTemplate.class));
                notifyDataSetChanged();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.toString());
            }
        });
    }

    @Override
    public DetailPersonAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.detail_person_row, parent, false);

        return new DetailPersonAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final DetailPersonAdapter.MyViewHolder holder, int position) {
        final Person person = personsList.get(position);
        holder.name.setText(person.getName());
        holder.function.setText(person.getFunctie());
        holder.email.setText(person.getEmail());
        holder.tel.setText(person.getTelefoon());
        holder.description.setText(person.getAantekeningen());
        if(isAnalyst){
            holder.edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(view.getContext(), InsertUpdatePerson.class);
                    intent.putExtra("updatingPerson", true);
                    intent.putExtra("templateKey", template.key);
                    intent.putExtra("templateName", template.getRole());
                    intent.putExtra("personKey", person.key);
                    Log.e(TAG, person.key);
                    view.getContext().startActivity(intent);
                }
            });
            holder.edit.setVisibility(View.VISIBLE);
        }

        imageViews.put(person.key, holder.image);

        String fileURL = "userImages/"+person.key+".jpg";

        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReferenceFromUrl("gs://actortemplateapp-10e30.appspot.com/").child(fileURL);

        try {
            final File localFile = File.createTempFile("images", "jpg");
            storageRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    Bitmap bitmap = BitmapFactory.decodeFile(localFile.getAbsolutePath());
                    holder.image.setImageBitmap(bitmap);

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                }
            });
        } catch (IOException e ) {}
    }

    @Override
    public int getItemCount() {
        return personsList.size();
    }

    private void updatePerson(final Person person){
        for (int i = 0; i < personsList.size(); i++){
            if(person.key.equals(personsList.get(i).key)){
                Person personInList = personsList.get(i);

                personInList.setName(person.getName());
                personInList.setFunctie(person.getFunctie());
                personInList.setEmail(person.getEmail());
                personInList.setTelefoon(person.getTelefoon());
                personInList.setAantekeningen(person.getAantekeningen());

                notifyDataSetChanged();
                notifyItemChanged(i);

                final ImageView image = imageViews.get(person.key);
                String fileURL = "userImages/"+person.key+".jpg";

                FirebaseStorage storage = FirebaseStorage.getInstance();
                StorageReference storageRef = storage.getReferenceFromUrl("gs://actortemplateapp-10e30.appspot.com/").child(fileURL);

                try {
                    final File localFile = File.createTempFile("images", "jpg");
                    storageRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                            Bitmap bitmap = BitmapFactory.decodeFile(localFile.getAbsolutePath());
                            image.setImageBitmap(bitmap);

                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                        }
                    });
                } catch (IOException e ) {}
            }
        }
    }
}
