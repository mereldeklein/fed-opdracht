package com.hu.fed.actortemplateapp;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Merel on 13-3-2017.
 */

public class MainTemplateAdapter extends RecyclerView.Adapter<MainTemplateAdapter.MyViewHolder> {

    private List<ActorTemplate> templatesList = new ArrayList<>();
    private DatabaseReference mFirebaseDatabaseReference;
    private String TAG = "MainTemplateAdapter";
    private Project project;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView name, description;
        public RecyclerView recyclerView;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.templateName);
            description = (TextView) view.findViewById(R.id.templateDescription);
            recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_person);
            RecyclerView.LayoutManager layoutManager = new CustomLinearLayoutManager(view.getContext());
            recyclerView.setLayoutManager(layoutManager);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int pos = getAdapterPosition();
            ActorTemplate template = templatesList.get(pos);
            Intent intent = new Intent(v.getContext(), ShowActorTemplate.class);
            intent.putExtra("templateKey", template.key);
            intent.putExtra("isAnalyst", project.isAnalyst());
            v.getContext().startActivity(intent);
        }
    }


    public MainTemplateAdapter(Project p) {
        project = p;

        final DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();

        mDatabase.child("templates").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                ActorTemplate template = dataSnapshot.getValue(ActorTemplate.class);
                template.key = dataSnapshot.getKey();

                //Alleen tonen bij project hoort en als status ACTIVE en niet ARCHIVED
                if(template.getProjectkey().equals(project.key) && template.getStatus().equals("ACTIVE")){
                    templatesList.add(template);
                    notifyDataSetChanged();
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                ActorTemplate template = dataSnapshot.getValue(ActorTemplate.class);
                template.key = dataSnapshot.getKey();

                if(dataSnapshot.getValue(ActorTemplate.class).getStatus().equals("ARCHIVED")){
                    removeTemplate(template);
                }
                else{
                    updateTemplate(template);
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                templatesList.remove( dataSnapshot.getValue(ActorTemplate.class));
                notifyDataSetChanged();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.toString());
            }
        });
    }

    @Override
    public MainTemplateAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.main_template_row, parent, false);

        return new MainTemplateAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MainTemplateAdapter.MyViewHolder holder, int position) {
        final ActorTemplate template = templatesList.get(position);
        holder.name.setText(template.getRole());
        holder.description.setText(template.getDescription());

        MainPersonAdapter mAdapter = new MainPersonAdapter(template);
        holder.recyclerView.setAdapter(mAdapter);
    }

    @Override
    public int getItemCount() {
        return templatesList.size();
    }

    public void removeTemplate(ActorTemplate template){
        for (int i = 0; i < templatesList.size(); i++){
            if(template.key.equals(templatesList.get(i).key)){
                templatesList.remove(i);
                notifyItemRemoved(i);
                notifyItemRangeChanged(i, templatesList.size());
            }
        }
    }

    public void updateTemplate(ActorTemplate template){
        for (int i = 0; i < templatesList.size(); i++){
            if(template.key.equals(templatesList.get(i).key)){
                ActorTemplate templateInList = templatesList.get(i);

                templateInList.setRole(template.getRole());
                templateInList.setDescription(template.getDescription());

                notifyDataSetChanged();
                notifyItemChanged(i);
            }
        }
    }
}
