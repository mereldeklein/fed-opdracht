package com.hu.fed.actortemplateapp;

/**
 * Created by Merel on 13-3-2017.
 */

public class Person {
    private String aantekeningen;
    private String email;
    private String functie;
    private String name;
    private String telefoon;
    private String templatekey;
    String key;

    public String getAantekeningen() {
        return aantekeningen;
    }

    public void setAantekeningen(String aantekeningen) {
        this.aantekeningen = aantekeningen;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFunctie() {
        return functie;
    }

    public void setFunctie(String functie) {
        this.functie = functie;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTemplatekey() {
        return templatekey;
    }

    public void setTemplatekey(String templatekey) {
        this.templatekey = templatekey;
    }

    public String getTelefoon() {
        return telefoon;
    }

    public void setTelefoon(String telefoon) {
        this.telefoon = telefoon;
    }
}
