package com.hu.fed.actortemplateapp;

import java.util.Map;

/**
 * Created by Merel on 4-3-2017.
 */

public class Project {

    private String name;
    private String description;
    private boolean analyst;
    private Map<String, String> analysts;
    private Map<String, String> team;
    String key;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isAnalyst() {
        return analyst;
    }

    public void setAnalyst(boolean analyst) {
        this.analyst = analyst;
    }

    public Map<String, String> getAnalysts() {
        return analysts;
    }

    public void setAnalysts(Map<String, String> analysts) {
        this.analysts = analysts;
    }

    public Map<String, String> getTeam() {
        return team;
    }

    public void setTeam(Map<String, String> team) {
        this.team = team;
    }
}
