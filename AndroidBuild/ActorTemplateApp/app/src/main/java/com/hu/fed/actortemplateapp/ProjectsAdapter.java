package com.hu.fed.actortemplateapp;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Merel on 4-3-2017.
 */

public class ProjectsAdapter extends RecyclerView.Adapter<ProjectsAdapter.MyViewHolder> {

    private List<Project> projectsList = new ArrayList<>();
    private DatabaseReference mFirebaseDatabaseReference;
    private String TAG = "ProjectsAdapter";

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView name, description;
        public FloatingActionButton analystEdit;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            description = (TextView) view.findViewById(R.id.description);
            analystEdit = (FloatingActionButton) view.findViewById(R.id.analyst_edit);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int pos = getAdapterPosition();
            Project project = projectsList.get(pos);
            Intent intent = new Intent(v.getContext(), ShowProject.class);
            intent.putExtra("key", project.key);
            v.getContext().startActivity(intent);
        }
    }


    public ProjectsAdapter() {
        final DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();

        mDatabase.child("projects").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Project p = dataSnapshot.getValue(Project.class);
                p.key = dataSnapshot.getKey();

                //Kijkt of de ingelogde gebruiker analyst of teamlid van het project is,
                // alleen dan wordt het project getoond in de lijst
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                if (user != null) {
                    String email = user.getEmail();
                    if(p.getAnalysts().containsValue(email)){
                        p.setAnalyst(true);
                        projectsList.add(p);
                        notifyDataSetChanged();
                    }
                    else if(p.getTeam().containsValue(email)){
                        p.setAnalyst(false);
                        projectsList.add(p);
                        notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                projectsList.remove( dataSnapshot.getValue(Project.class));
                notifyDataSetChanged();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.toString());
            }
        });
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.project_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Project project = projectsList.get(position);
        holder.name.setText(project.getName());
        holder.description.setText(project.getDescription());
        if(project.isAnalyst()){
            holder.analystEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(view.getContext(), ShowProject.class);
                    intent.putExtra("key", project.key);
                    view.getContext().startActivity(intent);
                }
            });
            holder.analystEdit.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return projectsList.size();
    }
}
