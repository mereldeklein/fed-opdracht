import { Injectable } from '@angular/core';
import {AngularFire} from "angularfire2";
import {Observable} from "rxjs";
import {Person} from "../models/Person";
import * as firebase from "firebase";

@Injectable()
export class PersonService {

  PATH = "/actoren"
  actoren$ = null;

  constructor(private af: AngularFire) {
    this.actoren$ = this.af.database.list(this.PATH)
  }

  getPeople(templatekey: string) : Observable<Person []>{
    console.log("In service");
    const regObserver : Observable<Person []> = this.af.database.list(this.PATH)
      .map(_people => _people.filter(person => person.templatekey == templatekey))
    console.log(regObserver);
    return regObserver;
  }

  getPerson(personkey: String) : Observable<Person>{
    const regObserver : Observable<Person> = this.af.database.object(this.PATH+"/"+personkey)
    console.log(regObserver);
    return regObserver;
  }

  newPerson(person: Person, file: any, url: any)
  {
    var newKey = firebase.database().ref().child('actoren').push().key;
    var updates = {};
    updates['/actoren/' + newKey] = person;
    firebase.database().ref().update(updates).then(function(){
      var storageRef = firebase.storage().ref();
      var imageRef = storageRef.child("userImages/"+newKey+".jpg");
      if(file.includes("data:image/")) {
        imageRef.putString(file, 'data_url').then(function () {
          window.location.href = url;
        })
      }
      else{
        window.location.href = url;
      }
    });
  }

  editPerson(person: Person, personkey: any, file: any, url: any)
  {
    var updates = {};
    updates['/actoren/' + personkey] = person;
    firebase.database().ref().update(updates).then(function(){
      var storageRef = firebase.storage().ref();
      var imageRef = storageRef.child("userImages/"+personkey+".jpg");
      if(file.includes("data:image/")) {
        imageRef.putString(file, 'data_url').then(function () {
          window.location.href = url;
        })
      }
      else{
        window.location.href = url;
      }
    });
  }
}
