import { Injectable } from '@angular/core';
import {AngularFire} from "angularfire2";
import {Observable} from "rxjs";
import {Project} from "../models/Project";

@Injectable()
export class ProjectlistService {

  PATH = "/projects"

  constructor(private af: AngularFire) { }

  getProjects() : Observable<Project []>{
    console.log("In service");
    const regObserver : Observable<Project []> = this.af.database.list(this.PATH)
    console.log(regObserver);
    return regObserver;
  }

  getProject(key : String) : Observable<Project>{
    const regObserver : Observable<Project> = this.af.database.object(this.PATH+"/"+key)
    console.log(regObserver);
    return regObserver;
  }
}
