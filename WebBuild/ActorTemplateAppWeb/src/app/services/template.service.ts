import { Injectable } from '@angular/core';
import {AngularFire} from "angularfire2";
import {Observable} from "rxjs";
import {Template} from "../models/Template";
import * as firebase from "firebase";

@Injectable()
export class TemplateService {

  PATH = "/templates"
  templates$ = null;

  constructor(private af: AngularFire) {
    this.templates$ = this.af.database.list(this.PATH)
  }

  getTemplates(projectkey: string) : Observable<Template []>{
    console.log("In service");
    const regObserver : Observable<Template []> = this.af.database.list(this.PATH)
      .map(_templates => _templates.filter(template => template.projectkey == projectkey && template.status == 'ACTIVE'))
    console.log(regObserver);
    return regObserver;
  }

  getTemplate(templatekey: String) : Observable<Template>{
    const regObserver : Observable<Template> = this.af.database.object(this.PATH+"/"+templatekey)
    console.log(regObserver);
    return regObserver;
  }

  newTemplate(template:Template, projectkey : string)
  {
    template.projectkey = projectkey;
    template.status = "ACTIVE";
    console.log(template);
    this.templates$.push(template)
  }

  editTemplate(template : Template, templatekey: any, url: any)
  {
    var updates = {};
    updates['/templates/' + templatekey] = template;
    firebase.database().ref().update(updates).then(function(){
        window.location.href = url;
    });
  }
}
