import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import {APP_BASE_HREF} from '@angular/common';
import {MaterialModule} from "@angular/material";

import { AppComponent } from './app.component';

import { AngularFireModule } from 'angularfire2';
import { LoginComponent } from './component/login/login.component';
import { EmailComponent } from './component/email/email.component';
import { SignupComponent } from './component/signup/signup.component';
import { MembersComponent } from './component/members/members.component';
import { ProjectlistComponent} from './component/projectlist/projectlist.component';
import { ProjectlistService } from "./services/projectlist.service";
import { TemplateService } from "./services/template.service";
import { PersonService } from "./services/person.service";

import { AuthGuard } from './auth.service';
import { routes } from './app.routes';
import {RouterModule} from "@angular/router";
import { ProjectComponent } from './component/project/project.component';
import { TemplateDetailComponent } from './component/template-detail/template-detail.component';
import { TemplateNewComponent } from './component/template-new/template-new.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { PersonNewComponent } from './component/person-new/person-new.component';
import { PersonDetailComponent } from './component/person-detail/person-detail.component';
import { TemplateEditComponent } from './component/template-edit/template-edit.component';

export const firebaseConfig = {
  apiKey: "AIzaSyB0h1F6GrSthtjN7CQ2RZxVTMrHEhF2vHA",
  authDomain: "actortemplateapp-10e30.firebaseapp.com",
  databaseURL: "https://actortemplateapp-10e30.firebaseio.com",
  storageBucket: "actortemplateapp-10e30.appspot.com",
  messagingSenderId: "342136073250"
};

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    EmailComponent,
    SignupComponent,
    MembersComponent,
    ProjectlistComponent,
    ProjectComponent,
    TemplateDetailComponent,
    TemplateNewComponent,
    PersonNewComponent,
    PersonDetailComponent,
    TemplateEditComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AngularFireModule.initializeApp(firebaseConfig),
    routes,
    RouterModule,
    MaterialModule,
    ReactiveFormsModule
  ],
  providers: [AuthGuard, {provide: APP_BASE_HREF, useValue : '/' }, ProjectlistService, TemplateService, PersonService],
  bootstrap: [AppComponent]
})
export class AppModule { }
