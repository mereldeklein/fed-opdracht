import { Component, OnInit } from '@angular/core';
import { AngularFire, AuthProviders, AuthMethods } from 'angularfire2';
import { Router } from '@angular/router';
import {moveIn, fallIn, moveInLeft} from "../../router.animations";
import {ProjectlistService} from "../../services/projectlist.service";
import {Project} from "../../models/Project";
import {OrderedProject} from "../../models/OrderedProject";
import {forEach} from "@angular/router/src/utils/collection";
import {Map} from "rxjs/util/Map";

@Component({
  selector: 'app-projectlist',
  templateUrl: './projectlist.component.html',
  styleUrls: ['./projectlist.component.css'],
  animations: [moveIn(), fallIn(), moveInLeft()],
  host: {'[@moveIn]': ''}
})
export class ProjectlistComponent implements OnInit {

  name: any;
  state: string = '';

  projects : Array<OrderedProject> = [];

  constructor(public af: AngularFire,private router: Router, private projService : ProjectlistService) {

    this.af.auth.subscribe(auth => {
      if(auth) {
        this.name = auth;
      }
    });

  }

  logout() {
    this.af.auth.logout();
    console.log('logged out');
    this.router.navigateByUrl('/login');
  }

  ngOnInit() {
    this.projService.getProjects().subscribe(res => this.order(res));
  }

  order(pro : Array<Project>){
    this.projects = [];
    for(var i = 0; i < pro.length; i++){
      let project : Project = pro[i];
      let rights = this.rights(project, this.name.auth.email);
      let ordered : OrderedProject = {isAnalyst : rights.isAnalyst, canView : rights.canView, project : project};
      this.projects.push(ordered);
    }
  }

  rights(project : Project, email : String) : {isAnalyst : boolean, canView: boolean} {
    let isAnalyst : boolean = false;
    let canView : boolean = false;

    for(var key in project.analysts){
      let value : String = project.analysts[key];
      if(value == email){
        isAnalyst = true;
        canView = true;
      }
    }

    if(!canView){
      for(var key in project.team){
        let value : String = project.team[key];
        if(value == email){
          canView = true;
        }
      }
    }

    return {isAnalyst, canView};
  }

  navigate(key : any){
    window.location.href = "/project/"+key;
  }

}
