import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {AngularFire} from "angularfire2";
import {ProjectlistService} from "../../services/projectlist.service";
import {Project} from "../../models/Project";
import {OrderedProject} from "../../models/OrderedProject";
import {moveInLeft, fallIn, moveIn} from "../../router.animations";
import {TemplateService} from "../../services/template.service";
import {Template} from "../../models/Template";
import {PersonService} from "../../services/person.service";
import {Person} from "../../models/Person";
import {EnrichedTemplate} from "../../models/EnrichedTemplate";
import storage = firebase.storage;
import {firebaseConfig} from "../../app.module";
import * as firebase from "firebase";

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css'],
  animations: [moveIn(), fallIn(), moveInLeft()],
  host: {'[@moveIn]': ''}
})
export class ProjectComponent implements OnInit {
  name: any;
  state: string = '';
  id : String;
  project : OrderedProject = {isAnalyst : false, canView : true, project : {
      name: "", description: "", team: new Map<string, string>(), analysts: new Map<string, string>()
    }
  };
  templateList : Array<EnrichedTemplate> = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public af: AngularFire,
    private projService : ProjectlistService,
    private tempService : TemplateService,
    private persService : PersonService) {
      this.af.auth.subscribe(auth => {
        if(auth) {
          this.name = auth;
        }
      })
  }

  logout() {
    this.af.auth.logout();
    console.log('logged out');
    this.router.navigateByUrl('/login');
  }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.projService.getProject(this.id).subscribe(res => this.loadProject(res));
  }

  loadProject(project : Project){
    let rights = this.rights(project, this.name.auth.email);
    this.project = {isAnalyst : rights.isAnalyst, canView : rights.canView, project : project};

    this.tempService.getTemplates(project.$key).subscribe(res => this.loadTemplates(res));
  }

  loadTemplates(templates : Template[]){
    this.templateList = [];

    for(var i = 0; i < templates.length; i++){
      let template = templates[i];
      let enrichedTemplate = {$key: template.$key, description: template.description, projectkey: template.projectkey, role: template.role, status: template.status, people: new Array<Person>()};
      this.persService.getPeople(template.$key).subscribe(res => this.loadEnriched(enrichedTemplate, res));
    }
  }

  loadEnriched(template: EnrichedTemplate, people: Person[]){
    template.people = people;
    this.templateList.push(template);

    for(var i = 0; i < people.length; i++) {
      let person = people[i];

      let storageRef = firebase.storage().ref();
      let spaceRef = storageRef.child('userImages/' + person.$key + ".jpg");

      storageRef.child('userImages/' + person.$key + ".jpg").getDownloadURL().then(function (url) {
        document.getElementById('profileImg' + person.$key).setAttribute("src", url)
      }).catch(function(error){});
    }
  }

  rights(project : Project, email : String) : {isAnalyst : boolean, canView: boolean} {
    let isAnalyst : boolean = false;
    let canView : boolean = false;

    for(var key in project.analysts){
      let value : String = project.analysts[key];
      if(value == email){
        isAnalyst = true;
        canView = true;
      }
    }

    if(!canView){
      for(var key in project.team){
        let value : String = project.team[key];
        if(value == email){
          canView = true;
        }
      }
    }

    return {isAnalyst, canView};
  }

  back(){
    window.location.href = "/projects";
  }

  new(){
    window.location.href = "/template-new/" + this.project.project.$key;
  }

  select(key: string){
    window.location.href = "/template/" + this.project.project.$key + "/" + key;
  }
}
