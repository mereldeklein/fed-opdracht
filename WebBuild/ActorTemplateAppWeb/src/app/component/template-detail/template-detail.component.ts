import { Component, OnInit } from '@angular/core';
import {moveIn, fallIn, moveInLeft} from "../../router.animations";
import {OrderedProject} from "../../models/OrderedProject";
import {ActivatedRoute, Router} from "@angular/router";
import {AngularFire} from "angularfire2";
import {ProjectlistService} from "../../services/projectlist.service";
import {Project} from "../../models/Project";
import {Template} from "../../models/Template";
import {Person} from "../../models/Person";
import {TemplateService} from "../../services/template.service";
import {PersonService} from "../../services/person.service";
import * as firebase from "firebase";

@Component({
  selector: 'app-template-detail',
  templateUrl: './template-detail.component.html',
  styleUrls: ['./template-detail.component.css'],
  animations: [moveIn(), fallIn(), moveInLeft()],
  host: {'[@moveIn]': ''}
})
export class TemplateDetailComponent implements OnInit {

  name: any;
  state: string = '';
  projectId : String;
  templateId: String;
  project : OrderedProject = {isAnalyst : false, canView : true, project : {
      name: "", description: "", team: new Map<string, string>(), analysts: new Map<string, string>()
    }
  };
  template : Template = {$key: "", description: "", projectkey: "", role: "", status: ""};
  people : Array<Person> = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public af: AngularFire,
    private projService : ProjectlistService,
    private  tempService : TemplateService,
    private persService : PersonService
  ) {
    this.af.auth.subscribe(auth => {
      if(auth) {
        this.name = auth;
      }
    })
  }

  logout() {
    this.af.auth.logout();
    console.log('logged out');
    this.router.navigateByUrl('/login');
  }

  ngOnInit() {
    this.projectId = this.route.snapshot.params['projectid'];
    this.templateId = this.route.snapshot.params['templateid'];

    this.projService.getProject(this.projectId).subscribe(res => this.loadProject(res));
    this.tempService.getTemplate(this.templateId).subscribe(res => this.template = res);
    this.persService.getPeople(this.templateId.toString()).subscribe(res => this.loadPeople(res));
  }

  loadProject(project : Project){
    let rights = this.rights(project, this.name.auth.email);
    this.project = {isAnalyst : rights.isAnalyst, canView : rights.canView, project : project};
    console.log(this.project);
  }

  loadPeople(people: Person[]){
    this.people = people;

    for(var i = 0; i < people.length; i++) {
      let person = people[i];

      let storageRef = firebase.storage().ref();
      let spaceRef = storageRef.child('userImages/' + person.$key + ".jpg");

      storageRef.child('userImages/' + person.$key + ".jpg").getDownloadURL().then(function (url) {
        document.getElementById('profileImg' + person.$key).setAttribute("src", url)
      }).catch(function(error){});
    }
  }

  rights(project : Project, email : String) : {isAnalyst : boolean, canView: boolean} {
    let isAnalyst : boolean = false;
    let canView : boolean = false;

    for(var key in project.analysts){
      let value : String = project.analysts[key];
      if(value == email){
        isAnalyst = true;
        canView = true;
      }
    }

    if(!canView){
      for(var key in project.team){
        let value : String = project.team[key];
        if(value == email){
          canView = true;
        }
      }
    }

    return {isAnalyst, canView};
  }

  back(){
    window.location.href = "/project/"+this.project.project.$key;
  }

  new(){
    window.location.href = "/person-new/"+this.project.project.$key+"/"+this.template.$key;
  }

  editPerson(personkey: string){
    window.location.href = "/person-edit/"+this.project.project.$key+"/"+this.template.$key+"/"+personkey;
  }

  editTemplate(templatekey: string){
    window.location.href = "/template-edit/"+this.project.project.$key+"/"+this.template.$key;
  }

  archiveren(templatekey: string){
    var updates = {};
    updates['/templates/'+templatekey+'/status'] = "ARCHIVED";
    firebase.database().ref().update(updates);
    window.location.href = "/project/"+this.project.project.$key;
  }
}
