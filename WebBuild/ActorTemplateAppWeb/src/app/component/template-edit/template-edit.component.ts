import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder} from "@angular/forms";
import {OrderedProject} from "../../models/OrderedProject";
import {Template} from "../../models/Template";
import {TemplateService} from "../../services/template.service";
import {ActivatedRoute, Router} from "@angular/router";
import {AngularFire} from "angularfire2";
import {ProjectlistService} from "../../services/projectlist.service";
import {Project} from "../../models/Project";

@Component({
  selector: 'app-template-edit',
  templateUrl: './template-edit.component.html',
  styleUrls: ['./template-edit.component.css']
})
export class TemplateEditComponent implements OnInit {

  form : FormGroup;
  name: any;
  state: string = '';
  projectId : String;
  templateId: String;
  project : OrderedProject = {isAnalyst : false, canView : true, project : {
    name: "", description: "", team: new Map<string, string>(), analysts: new Map<string, string>()
  }
  };
  template : Template = {$key: "", description: "", projectkey: "", role: "", status: ""};

  constructor(
    private fb: FormBuilder,
    private templateService : TemplateService,
    private route: ActivatedRoute,
    private router: Router,
    public af: AngularFire,
    private projService : ProjectlistService
  ) {
    this.af.auth.subscribe(auth => {
      if(auth) {
        this.name = auth;
      }
    })
  }

  ngOnInit() {
    this.projectId = this.route.snapshot.params['projectid'];
    this.templateId = this.route.snapshot.params['templateid'];

    this.form = this.fb.group({
      role: '',
      description: '',
      status: ''
    });

    this.projService.getProject(this.projectId).subscribe(res => this.loadProject(res));
    this.templateService.getTemplate(this.templateId).subscribe(res => this.loadTemplate(res));
  }

  logout() {
    this.af.auth.logout();
    console.log('logged out');
    this.router.navigateByUrl('/login');
  }

  loadProject(project : Project){
    let rights = this.rights(project, this.name.auth.email);
    this.project = {isAnalyst : rights.isAnalyst, canView : rights.canView, project : project};
    console.log(this.project);
  }

  loadTemplate(template : Template){
    this.template = template;
    this.buildForm();
  }

  rights(project : Project, email : String) : {isAnalyst : boolean, canView: boolean} {
    let isAnalyst : boolean = false;
    let canView : boolean = false;

    for(var key in project.analysts){
      let value : String = project.analysts[key];
      if(value == email){
        isAnalyst = true;
        canView = true;
      }
    }

    if(!canView){
      for(var key in project.team){
        let value : String = project.team[key];
        if(value == email){
          canView = true;
        }
      }
    }

    return {isAnalyst, canView};
  }

  buildForm(){
    this.form = this.fb.group({
      role: this.template.role,
      description: this.template.description,
      status: this.template.status
    });
  }

  back(){
    window.location.href = "/template/" + this.project.project.$key + "/" + this.template.$key;
  }

  edit(){
    let template : Template = this.form.value;
    template.projectkey = this.project.project.$key;
    let templatekey = this.template.$key;
    let url = "/template/" + this.project.project.$key + "/" + this.template.$key;

    this.templateService.editTemplate(template, templatekey, url);
  }
}
