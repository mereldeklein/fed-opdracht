import { Component, OnInit } from '@angular/core';
import {moveIn, fallIn, moveInLeft} from "../../router.animations";
import {Person} from "../../models/Person";
import {Project} from "../../models/Project";
import {PersonService} from "../../services/person.service";
import {TemplateService} from "../../services/template.service";
import {ProjectlistService} from "../../services/projectlist.service";
import {AngularFire} from "angularfire2";
import {Router, ActivatedRoute} from "@angular/router";
import {FormBuilder, FormGroup} from "@angular/forms";
import {Template} from "../../models/Template";
import {OrderedProject} from "../../models/OrderedProject";
import * as firebase from "firebase";

@Component({
  selector: 'app-person-detail',
  templateUrl: './person-detail.component.html',
  styleUrls: ['./person-detail.component.css'],
  animations: [moveIn(), fallIn(), moveInLeft()],
  host: {'[@moveIn]': ''}
})
export class PersonDetailComponent implements OnInit {
  form : FormGroup;
  name: any;
  state: string = '';
  projectId : String;
  templateId: String;
  personId: String;
  project : OrderedProject = {isAnalyst : false, canView : true, project : {
    name: "", description: "", team: new Map<string, string>(), analysts: new Map<string, string>()
  }
  };
  template : Template = {$key: "", description: "", projectkey: "", role: "", status: ""};
  person : Person = {$key: "", aantekeningen: "", email: "", functie: "", name: "", telefoon: "", templatekey: ""};

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    public af: AngularFire,
    private projService : ProjectlistService,
    private  tempService : TemplateService,
    private persService : PersonService
  ) {
    this.af.auth.subscribe(auth => {
      if(auth) {
        this.name = auth;
      }
    })
  }

  logout() {
    this.af.auth.logout();
    console.log('logged out');
    this.router.navigateByUrl('/login');
  }

  ngOnInit() {
    this.projectId = this.route.snapshot.params['projectid'];
    this.templateId = this.route.snapshot.params['templateid'];
    this.personId = this.route.snapshot.params['personid'];

    this.form = this.fb.group({
      aantekeningen: '',
      email: '',
      functie: '',
      name: '',
      telefoon: ''
    });

    this.projService.getProject(this.projectId).subscribe(res => this.loadProject(res));
    this.tempService.getTemplate(this.templateId).subscribe(res => this.template = res);
    this.persService.getPerson(this.personId).subscribe(res => this.loadPerson(res));
  }

  loadProject(project : Project){
    let rights = this.rights(project, this.name.auth.email);
    this.project = {isAnalyst : rights.isAnalyst, canView : rights.canView, project : project};
  }

  loadPerson(person: Person){
    this.person = person;

    let storageRef = firebase.storage().ref();
    let spaceRef = storageRef.child('userImages/' + this.person.$key + ".jpg");

    storageRef.child('userImages/' + this.person.$key + ".jpg").getDownloadURL().then(function (url) {
      document.getElementById('profileImg').setAttribute("src", url)
    }).catch(function(error){});

    this.buildForm();
  }

  rights(project : Project, email : String) : {isAnalyst : boolean, canView: boolean} {
    let isAnalyst : boolean = false;
    let canView : boolean = false;

    for(var key in project.analysts){
      let value : String = project.analysts[key];
      if(value == email){
        isAnalyst = true;
        canView = true;
      }
    }

    if(!canView){
      for(var key in project.team){
        let value : String = project.team[key];
        if(value == email){
          canView = true;
        }
      }
    }

    return {isAnalyst, canView};
  }

  buildForm(){
    this.form = this.fb.group({
      aantekeningen: this.person.aantekeningen,
      email: this.person.email,
      functie: this.person.functie,
      name: this.person.name,
      telefoon: this.person.telefoon
    });
  }

  back(){
    window.location.href = "/template/" + this.project.project.$key + "/" + this.template.$key;
  }

  edit(){
    let person : Person = this.form.value;
    person.templatekey = this.template.$key;
    let personkey = this.person.$key;
    let file = document.getElementById("profileImg").getAttribute("src");
    let url = "/template/" + this.project.project.$key + "/" + this.template.$key;

    this.persService.editPerson(person, personkey, file, url);
  }

  changeImage(input: any){
    let reader = new FileReader();
    this.readFile(input.files[0], reader, (result) =>{
      document.getElementById('profileImg').setAttribute("src", result);
    });
  }

  readFile(file, reader, callback){
    reader.onload = () => {
      callback(reader.result);
    }

    reader.readAsDataURL(file);
  }
}
