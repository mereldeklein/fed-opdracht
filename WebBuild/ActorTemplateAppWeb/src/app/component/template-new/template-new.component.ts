import { Component, OnInit } from '@angular/core';
import {OrderedProject} from "../../models/OrderedProject";
import {ActivatedRoute, Router} from "@angular/router";
import {AngularFire} from "angularfire2";
import {ProjectlistService} from "../../services/projectlist.service";
import {Project} from "../../models/Project";
import {FormGroup, FormBuilder} from "@angular/forms";
import {TemplateService} from "../../services/template.service";

@Component({
  selector: 'app-template-new',
  templateUrl: './template-new.component.html',
  styleUrls: ['./template-new.component.css']
})

export class TemplateNewComponent implements OnInit {

  form : FormGroup;
  name: any;
  state: string = '';
  projectId : String;
  project : OrderedProject = {isAnalyst : false, canView : true, project : {
    name: "", description: "", team: new Map<string, string>(), analysts: new Map<string, string>()
  }
  };


  constructor(
    private fb: FormBuilder,
    private templateService : TemplateService,
    private route: ActivatedRoute,
    private router: Router,
    public af: AngularFire,
    private projService : ProjectlistService
  ) {
    this.af.auth.subscribe(auth => {
      if(auth) {
        this.name = auth;
      }
    })
  }

  logout() {
    this.af.auth.logout();
    console.log('logged out');
    this.router.navigateByUrl('/login');
  }

  ngOnInit() {
    this.projectId = this.route.snapshot.params['projectid'];

    this.projService.getProject(this.projectId).subscribe(res => this.loadProject(res));

    this.buildForm()
  }

  buildForm(){
    this.form = this.fb.group({
      role: '',
      description: ''
    });
  }

  loadProject(project : Project){
    let rights = this.rights(project, this.name.auth.email);
    this.project = {isAnalyst : rights.isAnalyst, canView : rights.canView, project : project};
    console.log(this.project);
  }

  rights(project : Project, email : String) : {isAnalyst : boolean, canView: boolean} {
    let isAnalyst : boolean = false;
    let canView : boolean = false;

    for(var key in project.analysts){
      let value : String = project.analysts[key];
      if(value == email){
        isAnalyst = true;
        canView = true;
      }
    }

    if(!canView){
      for(var key in project.team){
        let value : String = project.team[key];
        if(value == email){
          canView = true;
        }
      }
    }

    return {isAnalyst, canView};
  }

  back(){
    window.location.href = "/project/" + this.project.project.$key;
  }

  new(){
    this.templateService.newTemplate(this.form.value, this.project.project.$key);
    console.log(this.form.value);
    this.form.reset();
    window.location.href = "/project/" + this.project.project.$key;
  }
}
