import { Component, OnInit } from '@angular/core';
import {moveIn, fallIn, moveInLeft} from "../../router.animations";
import {OrderedProject} from "../../models/OrderedProject";
import {Template} from "../../models/Template";
import {ActivatedRoute, Router} from "@angular/router";
import {AngularFire} from "angularfire2";
import {ProjectlistService} from "../../services/projectlist.service";
import {TemplateService} from "../../services/template.service";
import {PersonService} from "../../services/person.service";
import {Project} from "../../models/Project";
import {FormGroup, FormBuilder} from "@angular/forms";
import {Person} from "../../models/Person";

@Component({
  selector: 'app-person-new',
  templateUrl: './person-new.component.html',
  styleUrls: ['./person-new.component.css'],
  animations: [moveIn(), fallIn(), moveInLeft()],
  host: {'[@moveIn]': ''}
})
export class PersonNewComponent implements OnInit {
  form : FormGroup;
  name: any;
  state: string = '';
  projectId : String;
  templateId: String;
  project : OrderedProject = {isAnalyst : false, canView : true, project : {
    name: "", description: "", team: new Map<string, string>(), analysts: new Map<string, string>()
  }
  };
  template : Template = {$key: "", description: "", projectkey: "", role: "", status: ""};

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    public af: AngularFire,
    private projService : ProjectlistService,
    private  tempService : TemplateService,
    private persService : PersonService
  ) {
    this.af.auth.subscribe(auth => {
      if(auth) {
        this.name = auth;
      }
    })
  }

  logout() {
    this.af.auth.logout();
    console.log('logged out');
    this.router.navigateByUrl('/login');
  }

  ngOnInit() {
    this.projectId = this.route.snapshot.params['projectid'];
    this.templateId = this.route.snapshot.params['templateid'];

    this.projService.getProject(this.projectId).subscribe(res => this.loadProject(res));
    this.tempService.getTemplate(this.templateId).subscribe(res => this.template = res);

    this.buildForm();
  }

  loadProject(project : Project){
    let rights = this.rights(project, this.name.auth.email);
    this.project = {isAnalyst : rights.isAnalyst, canView : rights.canView, project : project};
  }

  rights(project : Project, email : String) : {isAnalyst : boolean, canView: boolean} {
    let isAnalyst : boolean = false;
    let canView : boolean = false;

    for(var key in project.analysts){
      let value : String = project.analysts[key];
      if(value == email){
        isAnalyst = true;
        canView = true;
      }
    }

    if(!canView){
      for(var key in project.team){
        let value : String = project.team[key];
        if(value == email){
          canView = true;
        }
      }
    }

    return {isAnalyst, canView};
  }

  buildForm(){
    this.form = this.fb.group({
      aantekeningen: '',
      email: '',
      functie: '',
      name: '',
      telefoon: ''
    });
  }

  back(){
    window.location.href = "/template/" + this.project.project.$key + "/" + this.template.$key;
  }

  new(){
    let person : Person = this.form.value;
    person.templatekey = this.template.$key;
    let file = document.getElementById("profileImg").getAttribute("src");
    let url = "/template/" + this.project.project.$key + "/" + this.template.$key;

    this.persService.newPerson(person, file, url);
  }

  changeImage(input: any){
    let reader = new FileReader();
    this.readFile(input.files[0], reader, (result) =>{
      document.getElementById('profileImg').setAttribute("src", result);
    });
  }

  readFile(file, reader, callback){
    reader.onload = () => {
      callback(reader.result);
    }

    reader.readAsDataURL(file);
  }
}
