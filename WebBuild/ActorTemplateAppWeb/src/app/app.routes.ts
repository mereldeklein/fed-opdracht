import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import {LoginComponent} from "./component/login/login.component";
import {SignupComponent} from "./component/signup/signup.component";
import {EmailComponent} from "./component/email/email.component";
import {MembersComponent} from "./component/members/members.component";
import {AuthGuard} from "./auth.service";
import {ProjectlistComponent} from "./component/projectlist/projectlist.component";
import {ProjectComponent} from "./component/project/project.component";
import {TemplateNewComponent} from "./component/template-new/template-new.component";
import {TemplateDetailComponent} from "./component/template-detail/template-detail.component";
import {PersonNewComponent} from "./component/person-new/person-new.component";
import {PersonDetailComponent} from "./component/person-detail/person-detail.component";
import {TemplateEditComponent} from "./component/template-edit/template-edit.component";


export const router: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'login-email', component: EmailComponent },
  { path: 'members', component: MembersComponent, canActivate: [AuthGuard] },
  { path: 'projects', component: ProjectlistComponent, canActivate: [AuthGuard]},
  { path: 'project/:id', component: ProjectComponent, canActivate: [AuthGuard]},
  { path: 'template/:projectid/:templateid', component: TemplateDetailComponent, canActivate: [AuthGuard]},
  { path: 'template-new/:projectid', component: TemplateNewComponent, canActivate: [AuthGuard]},
  { path: 'template-edit/:projectid/:templateid', component: TemplateEditComponent, canActivate: [AuthGuard]},
  { path: 'person-new/:projectid/:templateid', component: PersonNewComponent, canActivate: [AuthGuard]},
  { path: 'person-edit/:projectid/:templateid/:personid', component: PersonDetailComponent, canActivate: [AuthGuard]}
]

export const routes: ModuleWithProviders = RouterModule.forRoot(router);
