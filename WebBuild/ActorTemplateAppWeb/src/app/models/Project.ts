/**
 * Created by Merel on 22-3-2017.
 */
export interface Project{

  $key?: string
  name: string,
  description: string,
  team: Map<string, string>,
  analysts: Map<string, string>
}
