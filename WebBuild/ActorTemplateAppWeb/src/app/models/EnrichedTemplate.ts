import {Template} from "./Template";
import {Person} from "./Person";
/**
 * Created by Merel on 26-3-2017.
 */
export interface EnrichedTemplate extends Template{

  $key?: string
  people: Array<Person>
}
