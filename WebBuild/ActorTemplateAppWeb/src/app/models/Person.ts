/**
 * Created by Merel on 26-3-2017.
 */
export interface Person{

  $key?: string
  aantekeningen: string,
  email: string,
  functie: string,
  name: string,
  telefoon: string,
  templatekey: string
}
