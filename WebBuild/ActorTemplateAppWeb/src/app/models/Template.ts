/**
 * Created by Merel on 26-3-2017.
 */
export interface Template{

  $key?: string
  description: string,
  projectkey: string,
  role: string,
  status: string
}
