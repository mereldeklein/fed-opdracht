import {Project} from "./Project";
/**
 * Created by Merel on 22-3-2017.
 */
export interface OrderedProject{

  isAnalyst : boolean,
  canView: boolean,
  project: Project
}
