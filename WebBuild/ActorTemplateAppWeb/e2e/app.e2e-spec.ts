import { ActorTemplateAppWebPage } from './app.po';

describe('actor-template-app-web App', () => {
  let page: ActorTemplateAppWebPage;

  beforeEach(() => {
    page = new ActorTemplateAppWebPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
